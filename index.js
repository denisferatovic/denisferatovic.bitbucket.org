
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var populationUrl = 'http://api.population.io:80/1.0/life-expectancy/remaining/';

var username = "ois.seminar";
var password = "ois4fri";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
*/

function generiranje() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
}
 
function generirajPodatke(stPacienta) {
    ehrId = "";
    if(stPacienta == 1){
        kreirajEHR("Janez", "Novak", "m", "1955-04-05");
    }
    else if(stPacienta == 2){
        kreirajEHR("Marko", "Kodišek", "m", "1998-02-05");
    }
    else if(stPacienta == 3){
        kreirajEHR("Ana", "Kern", "z", "1975-06-02")
    }
   
  return ehrId;
}
 
function dodajMeritveVZ(ehrId, datumInUra, visina, teza) {
    sessionId = getSessionId();
 
    if (!ehrId || ehrId.trim().length == 0) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        var podatki = {
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": datumInUra,
            "vital_signs/height_length/any_event/body_height_length": visina,
            "vital_signs/body_weight/any_event/body_weight": teza,
            "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
            "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,

        };
        var parametriZahteve = {
            ehrId: ehrId,
            templateId: 'Vital Signs',
            format: 'FLAT',
            committer: "",
        };
        $.ajax({
            url: baseUrl + "/composition?" + $.param(parametriZahteve),
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(podatki),
            success: function (res) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
            },
            error: function(err) {
                $("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
            }
        });
    }
}

var ehrIdNovak="";
var ehrIdKodisek="";
var ehrIdKern="";
 
function kreirajEHR(ime, priimek, spol, datumRojstva) {
  
    sessionId = getSessionId();
  if(ime == "Janez Novak"){
    var ehrId= "5911lu30-3a64-4237-8739-b52d9013c95e";

  } else if(ime == "Marko Kodišek"){
    var ehrId= "62d678d8-b344-4567-aec2-440d854gud34";
  }
  else if(ime == "Ana Kern"){
    var ehrId= "c8b5ga390-d01n-468b-98de-44dme4352a74";
  }
  
  
  if(spol=="m"){
    spol = "MALE";
  } 
  if(spol=="z"){
    spol = "FEMALE";
  }
  
    var datumRojstva = datumRojstva + "T00:00:00.000Z";
 
    if (!ime || !priimek || !spol || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || spol.trim().length == 0 || datumRojstva.trim().length == 0) {
        $("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        $.ajaxSetup({
            headers: {"Ehr-Session": sessionId}
        });
        $.ajax({
            url: baseUrl + "/ehr",
            type: 'POST',
            success: function (data) {
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    gender: spol,
                    dateOfBirth: datumRojstva,
                    partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
                };
                    var datimInUra, visina, teza;
                $.ajax({
                    url: baseUrl + "/demographics/party",
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify(partyData),
                    success: function (party) {
                        if (party.action == 'CREATE') {
                            $("#kreirajSporocilo").append("<br>Ponovno generiran EHR: " + ehrId);
                            $("#preberiEHRid").val(ehrId);
                        }
                            if(ime=="Janez Novak"){
                                ehrIdNovak=ehrId;
                                teza= "75.0";
                                visina="183.0";
                                datumInUra= "2016-01-04T16:30Z";
                                dodajMeritveVZ(ehrIdNovak, datumInUra, visina, teza);
                                teza= "78.0";
                                visina="185.0";
                                datumInUra= "2017-01-04T16:30Z";
                                dodajMeritveVZ(ehrIdNovak, datumInUra, visina, teza);
                                teza="80.0";
                                visina="187.0";
                                datumInUra= "2018-01-04T16:30Z";
                                dodajMeritveVZ(ehrIdNovak, datumInUra, visina, teza);
                            }
                            else if(ime=="Marko Kodišek"){
                                ehrIdKodisek=ehrId;
                                teza= "72.0";
                                visina="180.0";
                                datumInUra= "2016-02-05T20:15Z";
                                dodajMeritveVZ(ehrIdKodisek, datumInUra, visina, teza);
                                teza= "76.0";
                                visina="188.0";
                                datumInUra= "2017-02-05T20:15Z";
                                dodajMeritveVZ(ehrIdKodisek, datumInUra, visina, teza);
                                teza="72.0";
                                visina="195.0";
                                datumInUra= "2018-02-05T20:15Z";
                                dodajMeritveVZ(ehrIdKodisek, datumInUra, visina, teza);
                            }
                    
                            else if(ime=="Ana Kern"){
                                ehrIdKern=ehrId;
                                teza= "40.0";
                                visina="176.0";
                                datumInUra= "2018-01-01T12:45Z";
                                dodajMeritveVZ(ehrIdKern, datumInUra, visina, teza);
                                teza= "45.0";
                                visina="177.0";
                                datumInUra= "2018-05-01T14:45Z";
                                dodajMeritveVZ(ehrIdKern, datumInUra, visina, teza);
                                teza="41.5";
                                visina="178.0";
                                datumInUra= "2018-06-01T14:45Z";
                                dodajMeritveVZ(ehrIdKern, datumInUra, visina, teza);
                            }
                      },
                      error: function(err) {
                          $("#kreirajSporocilo").html("<span class='obvestilo label " +
                      "label-danger fade-in'>Napaka '" +
                      JSON.parse(err.responseText).userMessage + "'!");
                      }
                  });
            }
          });
      }
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function kreirajEHRzaBolnika() {
  sessionId = getSessionId();
  
  
  var ime = $("#kreirajIme").val();
  var priimek = $("#kreirajPriimek").val();
  var spol = $("#kreirajSpol").val();
  var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
  
  if(spol=="m"){
    spol = "MALE";
  } 
  if(spol=="z"){
    spol = "FEMALE";
  }
  
  if (!ime || !priimek || !datumRojstva || !spol || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || spol.trim().length == 0) {
    $("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
  } else {
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            var partyData = {
                firstNames: ime,
                lastNames: priimek,
                gender: spol,
                dateOfBirth: datumRojstva,
                partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
                        $("#preberiEHRid").val(ehrId);
                    }
                },
                error: function(err) {
                  $("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
                }
            });
        }
    });
  }
}


/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
  sessionId = getSessionId();

  var ehrId = $("#dodajVitalnoEHR").val();
  var datumInUra = $("#dodajVitalnoDatumInUra").val();
  var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
  var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
  var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
  var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
  if (!ehrId || ehrId.trim().length == 0) {
    $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
  } else {
    $.ajaxSetup({
        headers: {"Ehr-Session": sessionId}
    });
    var podatki = {
      // Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
        "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
        "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    };
    var parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: merilec
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        success: function (res) {
            $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
        },
        error: function(err) {
          $("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
        }
    });
  }
}
function preberiMeritveVitalnihZnakov() {
  sessionId = getSessionId();

  var ehrId = $("#meritveVitalnihZnakovEHRid").val();
  var tip = $("#preberiTipZaVitalneZnake").val();

  if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
    $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
  } else {
    $.ajax({
      url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {"Ehr-Session": sessionId},
        success: function (data) {
        var party = data.party;
        $("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> pacienta <b></b>.</span><br/><br/>");
                var spol = party.gender;
                var rodil = party.dateOfBirth;
            
        if (tip == "telesna teža") {
          $.ajax({
              url: baseUrl + "/view/" + ehrId + "/" + "weight",
              type: 'GET',
              headers: {"Ehr-Session": sessionId},
              success: function (res) {
                if (res.length > 0) {
                  var results = "<table class='table table-striped " + "table-hover'><tr><th>Datum in ura</th>" + "<th class='text-right'>Telesna teža</th></tr>";
                          
                          var tabela = new Array(res.length);
                          var st = res.length -1;
                    for (var i in res) {
                        results += "<tr><td>" + res[i].time + "</td><td class='text-right'>" + res[i].weight + " "  + res[i].unit + "</td></tr>";
                            var time =  res[i].time.substring(0, 10);
                        tabela[st-i] = {
                                "datum": time,
                                "visina": res[i].weight,
                              };    
                    }
                    results += "</table>";
                    
                    function labelFunction(item, label) {
                    if (item.index === item.graph.chart.dataProvider.length - 1)
                      return label;
                    else
                      return "";
                  }
                  
                  var chart = AmCharts.makeChart("chartdiv", {
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": tabela,
                    "graphs": [{
                      "lineThickness": 3,
                      "labelText": "[[title]]",
                      "labelFunction": labelFunction,
                      "labelPosition": "right",
                      "title": "Teža",
                      "type": "smoothedLine",
                      "lineColor": "#337ab7",
                      "valueField": "visina"
                    }],
                    "categoryField": "datum",
                    "categoryAxis": {
                      "gridPosition": "višina",
                      "gridAlpha": 0,
                    }
                  
                  });
                  
                    $("#rezultatMeritveVitalnihZnakov").append(results);
                    
                } else {
                  $("#preberiMeritveVitalnihZnakovSporocilo").html(
                          "<span class='obvestilo label label-warning fade-in'>" +
                          "Ni podatkov!</span>");
                  }
                },
                error: function() {
                  $("#preberiMeritveVitalnihZnakovSporocilo").html(
                        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                        JSON.parse(err.responseText).userMessage + "'!");
                }
            });
              
        } else if (tip == "telesna višina") {
          $.ajax({
              url: baseUrl + "/view/" + ehrId + "/" + "height",
              type: 'GET',
              headers: {"Ehr-Session": sessionId},
              success: function (res) {
                if (res.length > 0) {
                  var tabela = new Array(res.length);
                  var results = "<table class='table table-striped " + "table-hover'><tr><th>Datum in ura</th>" + "<th class='text-right'>Telesna višina</th></tr>";
                var st = res.length -1;
                    for (var i in res) {
                        results += "<tr><td>" + res[i].time + "</td><td class='text-right'>" + res[i].height + " "  + res[i].unit + "</td></tr>";
                          
                              var time =  res[i].time.substring(0, 10);
                        tabela[st-i] = {
                              "datum": time,
                              "visina": res[i].height,
                            };
                          
                    }
                    results += "</table>";

                      function labelFunction(item, label) {
                      if (item.index === item.graph.chart.dataProvider.length - 1)
                        return label;
                      else
                        return "";
                    }
                    
                    var chart = AmCharts.makeChart("chartdiv", {
                      "type": "serial",
                      "theme": "light",
                      "dataProvider": tabela,
                      "graphs": [{
                        "lineThickness": 3,
                        "labelText": "[[title]]",
                        "labelFunction": labelFunction,
                        "labelPosition": "right",
                        "title": "Višina",
                        "type": "smoothedLine",
                        "lineColor": "#337ab7",
                        "valueField": "visina"
                      }],
                      "categoryField": "datum",
                      "categoryAxis": {
                        "gridPosition": "višina",
                        "gridAlpha": 0,
                      }
                    
                    });
                    
                    $("#rezultatMeritveVitalnihZnakov").append(results);
                    
                } else {
                  $("#preberiMeritveVitalnihZnakovSporocilo").html(
                          "<span class='obvestilo label label-warning fade-in'>" +
                          "Ni podatkov!</span>");
                }
              },
              error: function() {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                        JSON.parse(err.responseText).userMessage + "'!");
              }
          });
          
        } else if (tip == "izračun ITT"){
            var results1 = [];
          $.ajax({
              url: baseUrl + "/view/" + ehrId + "/" + "weight",
              type: 'GET',
              headers: {"Ehr-Session": sessionId},
              success: function (res) {
                if (res.length > 0) {
                    for (var i in res) {
                        results1[i] = res[i].weight;
                    }
                } else {
                  $("#preberiMeritveVitalnihZnakovSporocilo").html(
                          "<span class='obvestilo label label-warning fade-in'>" +
                          "Ni podatkov!</span>");
                }
                
                $.ajax({
                url: baseUrl + "/view/" + ehrId + "/" + "height",
                type: 'GET',
                headers: {"Ehr-Session": sessionId},
                success: function (res) {
                  if (res.length > 0) {
                    var tabela = new Array(res.length);
                    var results = "<table class='table table-striped " + "table-hover'><tr><th>Datum in ura</th>" + "<th class='text-right'>Inteks telesne mase</th></tr>";
                    var st = res.length -1;
                      for (var i in res) {
                          results += "<tr><td>" + res[i].time + "</td><td class='text-right'>" + (results1[i]/((res[i].height/100)*(res[i].height/100))) + " "+ "kg/m^2" + "</td></tr>";
                          
                        var time =  res[i].time.substring(0, 10);
                          tabela[st-i] = {
                                  "datum": time,
                                  "visina": (results1[i]/((res[i].height/100)*(res[i].height/100))),
                                };
                                
                              }
                              
                      results += "</table>";
                        function labelFunction(item, label) {
                       if (item.index === item.graph.chart.dataProvider.length - 1)
                          return label;
                        else
                          return "";
                      }
                      
                      var chart = AmCharts.makeChart("chartdiv", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": tabela,
                        "graphs": [{
                          "lineThickness": 3,
                          "labelText": "[[title]]",
                          "labelFunction": labelFunction,
                          "labelPosition": "right",
                          "title": "BMI",
                          "type": "smoothedLine",
                          "lineColor": "#337ab7",
                          "valueField": "visina"
                        }],
                        "categoryField": "datum",
                        "categoryAxis": {
                          "gridPosition": "višina",
                          "gridAlpha": 0,
                        }
                      
                    });

                      $("#rezultatMeritveVitalnihZnakov").append(results);
                  } else {
                    $("#preberiMeritveVitalnihZnakovSporocilo").html(
                            "<span class='obvestilo label label-warning fade-in'>" +
                            "Ni podatkov!</span>");
                  }
                },
                error: function() {
                  $("#preberiMeritveVitalnihZnakovSporocilo").html(
                          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                          JSON.parse(err.responseText).userMessage + "'!");
                }
            });
              
              },
              error: function() {
                $("#preberiMeritveVitalnihZnakovSporocilo").html(
                        "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                        JSON.parse(err.responseText).userMessage + "'!");
              }
          });
        }
        },
        error: function(err) {
          $("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
        }
    });
  }
}
$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
  $("#kreirajSpol").val(podatki[2]);
    $("#kreirajDatumRojstva").val(podatki[3]);
    
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
  $('#preberiObstojeciEHR').change(function() {
    $("#preberiSporocilo").html("");
    $("#preberiEHRid").val($(this).val());
  });

  /**
   * Napolni testne vrednosti (EHR ID, datum in ura, telesna višina,
   * telesna teža, telesna temperatura, sistolični in diastolični krvni tlak,
   * nasičenost krvi s kisikom in merilec) pri vnosu meritve vitalnih znakov
   * bolnika, ko uporabnik izbere vrednosti iz padajočega menuja (npr. Ata Smrk)
   */
  $('#preberiObstojeciVitalniZnak').change(function() {
    $("#dodajMeritveVitalnihZnakovSporocilo").html("");
    var podatki = $(this).val().split("|");
    $("#dodajVitalnoEHR").val(podatki[0]);
    $("#dodajVitalnoDatumInUra").val(podatki[1]);
    $("#dodajVitalnoTelesnaVisina").val(podatki[2]);
    $("#dodajVitalnoTelesnaTeza").val(podatki[3]);
    $("#dodajVitalnoMerilec").val(podatki[4]);
  });

  /**
   * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
   * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Ata Smrk, Pujsa Pepa)
   */
  $('#preberiEhrIdZaVitalneZnake').change(function() {
    $("#preberiMeritveVitalnihZnakovSporocilo").html("");
    $("#rezultatMeritveVitalnihZnakov").html("");
    $("#meritveVitalnihZnakovEHRid").val($(this).val());
  });

});



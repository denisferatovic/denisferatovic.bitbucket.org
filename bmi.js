window.addEventListener('load', function() {

  function bmi() {
    var visina = document.getElementById('visina').value;
    var teza = document.getElementById('teza').value;
    //izracuna bmi
    var bmiRezultat = teza/(visina * visina);
    //alert(bmiRezultat);
    if (bmiRezultat < 15) {
      alert("Izredno prenizka telesna teža");   }

    else if (bmiRezultat < 16 && bmiRezultat > 15) {
      alert("Resno prenizka telesna teža");
    }

    else if (bmiRezultat <18.5 && bmiRezultat >= 16) {
      alert("Prenizka telesna teža");
    }
    else if (bmiRezultat <= 25  && bmiRezultat > 18.5) {
      alert("Normalno");
    }
    else if (bmiRezultat <30 && bmiRezultat > 25) {
      alert("Prekomerna telesna teža");
    }
    else if (bmiRezultat < 35 && bmiRezultat > 30) {
      alert("Zmerna prekomerna telesna teža");
    }
    else if (bmiRezultat < 40 && bmiRezultat > 35) {
      alert("Resna prekomerna telesna teža");
    }
    else if (bmiRezultat > 40) {
      alert("Izredna prekomerna telesna teža");
    }

    else {
      alert("Prosimo vnesite pravilne vrednosti");
      
    }
    
  }

  document.getElementById('gumb').addEventListener("click", bmi);

  /*var LojzeNovak = {
    visina: "1.8",
    teza: "75",
    spol: "moski",
  }

  var MarkoKodisek = {
    visina: "1.95",
    teza: "120",
    spol: "moski",
  }

  var AnaKern = {
    visina: "1.5",
    teza: "35",
    spol: "zenska",
  }*/
function preveriPritisk() {
    var diastolic = document.getElementById("diastolic").value;
    var systolic = document.getElementById("systolic").value;

    if (systolic < 90 && diastolic < 60) {
      alert("Vaš krvni pritisk je prenizek");
    }
    else if (systolic > 90 && systolic <= 120 && diastolic > 60 && diastolic < 80) {
      alert("Vaš krvni pritisk je idealen");
    }
    else if (systolic > 120 && systolic < 140 && diastolic > 80 && diastolic < 90) {
      alert("Vaš krvni pritisk je previsok");
    }
    else if (systolic > 140 && diastolic > 90) {
      alert("Vaš krvni pritisk je kritično visok");
    }

    else {
      alert("Prosimo vnestite pravilne vrednosti");
    }
  }
  document.getElementById("tlak").addEventListener("click", preveriPritisk);
  
});